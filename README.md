# Artifact Repository for paper: "JsRipper: Enabling In-Browser, Signature-Based Tracking Function Detection"

This repository contains three directories.

- `jsripper` contains the function signature generation library, along with a couple of tools to
  classify the generated signatures according to the function source code.
- `crawl_jsripper` contains the crawler used to collect the external scripts used in the study and
  the data analysis notebook.
- `data` contains a YAML file with functions marked as tracking as part of our classification;
  client-side storage tracking functions are especially easy to block and replace with no-operation
  shims.

## License

Copyright (c) 2023 Inria
