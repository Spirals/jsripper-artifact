#![deny(clippy::pedantic)]

use std::{
    collections::HashMap,
    fs, io,
    path::{Path, PathBuf},
};

use serde::{Deserialize, Serialize};
use thiserror::Error;

#[allow(clippy::doc_markdown)]
/// Error type for JsRipper configuration loading.
#[derive(Debug, Error)]
pub enum Error {
    /// Error when opening the configuration file.
    #[error("could not open the configuration file expected to be at `{path}`")]
    Io { path: PathBuf, source: io::Error },
    /// Error when parsing the TOML configuration file.
    #[error("could not parse the configuration file found at `{path}`")]
    ParseError {
        path: PathBuf,
        source: toml::de::Error,
    },
}

#[allow(clippy::doc_markdown)]
/// JsRipper configuration.
///
/// Created from a TOML file using [`Config::read_from_file()`].
#[derive(Debug, Clone, Serialize, Deserialize)]
#[serde(deny_unknown_fields)]
pub struct Config {
    /// Paths to filter list files used to flag downloaded script files.
    pub filter_lists: Vec<PathBuf>,
    // `regex` crate regexes used to filter the functions based on their source code.
    // Only functions bodies having a match with one of these regexes will be shown to be
    // labeled.
    pub candidate_function_regexes: HashMap<String, Vec<String>>,
    /// Functions to exclude from labeling.
    pub allowlist: AllowlistConfig,
}

impl Default for Config {
    fn default() -> Self {
        let mut candidate_function_regexes = HashMap::new();
        candidate_function_regexes.insert(
            String::from("client_side_storage"),
            vec![
                String::from("\\.cookie\\b"),
                String::from("\\blocalStorage\\b"),
            ],
        );
        candidate_function_regexes.insert(
            String::from("direct_network_requests"),
            vec![String::from("XMLHttpRequest")],
        );

        Self {
            filter_lists: vec![
                PathBuf::from("easylist.txt"),
                PathBuf::from("easyprivacy.txt"),
            ],
            candidate_function_regexes,
            allowlist: AllowlistConfig {
                url_patterns: vec![String::from("%code.jquery.com%jquery%")],
                source_regexes: Vec::new(),
            },
        }
    }
}

impl Config {
    #[allow(clippy::doc_markdown)]
    /// Read JsRipper configuration from a file `jsripper.toml` in the provided directory.
    ///
    /// # Errors
    /// - Returns [`Error::Io`] if the configuration file cannot be found.
    /// - Returns [`Error::ParseError`] if the TOML configuration file cannot be parsed.
    pub fn read_from_file(dir: &Path) -> Result<Self, Error> {
        let path = dir.join("jsripper.toml");

        let toml_file = fs::read_to_string(&path).map_err(|source| Error::Io {
            path: path.clone(),
            source,
        })?;

        let config: Self =
            toml::from_str(&toml_file).map_err(|source| Error::ParseError { path, source })?;
        Ok(config)
    }
}

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct AllowlistConfig {
    /// SQL `LIKE` patterns matching URLs of scripts to exclude from labeling.
    pub url_patterns: Vec<String>,
    /// Regexes matching source code of functions to exclude from labeling.
    pub source_regexes: Vec<String>,
}
