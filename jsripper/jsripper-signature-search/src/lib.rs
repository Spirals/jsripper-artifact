#![deny(clippy::pedantic)]
#![allow(clippy::missing_panics_doc)]

mod signature_db;

use std::{
    fs,
    os::unix::fs::MetadataExt,
    path::Path,
    path::PathBuf,
    sync::{
        atomic::{AtomicU64, Ordering},
        Arc,
    },
    time::Instant,
};

use adblock::{
    engine::Engine as AdBlockerEngine,
    lists::{FilterSet, ParseOptions},
};
use log::trace;
use log::{debug, error, info};
use sha2::Digest;
use signature_db::SignatureDb;
use tokio::{
    sync::{Mutex, Semaphore},
    task::JoinSet,
};
use url::Url;

use jsripper_function_processing::{
    function_extraction::Script, function_signature::generate_function_ast_signature,
};

use crate::signature_db::FunctionSignature;

#[allow(clippy::doc_markdown)]
/// Generate the signature of all functions from the given script file paths, flag whether they
/// are blocked by the given filter lists, and write this into a SQLite database at the given path.
#[allow(clippy::too_many_lines)]
pub async fn generate_function_signatures(
    js_file_paths: &[PathBuf],
    filter_list_paths: &[PathBuf],
    db_path: &Path,
) {
    let signature_db = Arc::new(SignatureDb::new(db_path).await.unwrap());

    let ad_blocker_engine = Arc::new(init_filter_list_blocker(filter_list_paths));
    info!("Loaded filter lists.");

    let file_counter = Arc::new(AtomicU64::new(0));
    let function_counter = Arc::new(AtomicU64::new(0));
    let last_function_time = Arc::new(Mutex::new(Instant::now()));

    let concurrent_file_semaphore = Arc::new(Semaphore::new(500));

    let mut tasks = JoinSet::new();

    for js_file_path in js_file_paths {
        let concurrent_file_semaphore = Arc::clone(&concurrent_file_semaphore);
        let permit = concurrent_file_semaphore.acquire_owned().await.unwrap();

        trace!(
            "Starting JS file [{}] `{}`",
            file_counter.load(Ordering::Relaxed),
            js_file_path.display()
        );

        file_counter.fetch_add(1, Ordering::Relaxed);

        let function_counter = Arc::clone(&function_counter);
        let last_function_time = Arc::clone(&last_function_time);
        let ad_blocker_engine = Arc::clone(&ad_blocker_engine);
        let signature_db = Arc::clone(&signature_db);
        let js_file_path = js_file_path.clone();

        tasks.spawn(async move {
            process_js_file(
                &js_file_path,
                function_counter,
                last_function_time,
                ad_blocker_engine,
                &signature_db,
            )
            .await;
            drop(permit);
        });
    }

    while let Some(_res) = tasks.join_next().await {}
}

fn init_filter_list_blocker(filter_list_paths: &[PathBuf]) -> AdBlockerEngine {
    let mut filter_set = FilterSet::new(false);
    for list_file_path in filter_list_paths {
        let list = fs::read_to_string(list_file_path).unwrap();
        filter_set.add_filter_list(&list, ParseOptions::default());
    }

    AdBlockerEngine::from_filter_set(filter_set, true)
}

async fn process_js_file(
    js_file_path: &Path,
    function_counter: Arc<AtomicU64>,
    last_function_time: Arc<Mutex<Instant>>,
    ad_blocker_engine: Arc<AdBlockerEngine>,
    signature_db: &SignatureDb,
) {
    let last_function_time = Arc::clone(&last_function_time);
    let signature_db = signature_db.clone();
    // let ad_blocker_engine = Arc::clone(&ad_blocker_engine);

    let page_domain = js_file_path
        .parent()
        .unwrap()
        .parent()
        .unwrap()
        .file_name()
        .unwrap()
        .to_string_lossy()
        .to_string();
    let resource_url = js_file_path
        .file_name()
        .unwrap()
        .to_string_lossy()
        .replace('\x1a', "/"); // Replace SUB characters

    let is_first_party = if let Ok(resource_url) = Url::parse(&resource_url) {
        resource_url.host_str() == Some(&page_domain)
    } else {
        false
    };

    let resource_in_filter_lists = ad_blocker_engine
        .check_network_urls(&resource_url, &format!("http://{page_domain}/"), "script")
        .matched;

    let js_file = fs::read_to_string(js_file_path).unwrap();

    let resource_byte_size = fs::metadata(js_file_path).unwrap().size();

    let resource_sha256 = sha256_str(&js_file);
    debug!(
        "SHA-256 of file `{}`: {resource_sha256}",
        js_file_path.display(),
    );

    let functions = if let Ok(script) = Script::parse(&js_file) {
        if let Ok(functions) = script.functions() {
            functions
        } else {
            error!(
                "Error while extracting functions from {}",
                js_file_path.display(),
            );
            return;
        }
    } else {
        error!(
            "Error while extracting functions from {}",
            js_file_path.display(),
        );
        return;
    };
    debug!(
        "{}\u{a0}functions found in file `{}`",
        functions.len(),
        js_file_path.display(),
    );

    for function in functions {
        let sig = generate_function_ast_signature(&function);

        let function_span = function.span;

        let function_byte_start = function_span.lo.0 - 1;
        let function_body_byte_start = function.body.unwrap().span.lo.0 - 1;
        let function_byte_end = function_span.hi.0 - 1;

        let function_signature = FunctionSignature::new(
            &page_domain,
            js_file_path,
            &resource_url,
            &resource_sha256,
            resource_in_filter_lists,
            is_first_party,
            resource_byte_size.try_into().unwrap(),
            function_byte_start,
            function_body_byte_start,
            function_byte_end,
            &sig,
        );

        signature_db
            .write_signature(&function_signature)
            .await
            .unwrap();

        function_counter.fetch_add(1, Ordering::Relaxed);

        let function_display_count = 10000;

        if function_counter.load(Ordering::Relaxed) % function_display_count == 0 {
            let mut last_function_time_guard = last_function_time.lock().await;
            info!(
                "Handled {function_display_count}\u{a0}functions in {:?}",
                last_function_time_guard.elapsed(),
            );
            *last_function_time_guard = Instant::now();
        }
    }
}

fn sha256_str(string: &str) -> String {
    let mut hasher = sha2::Sha256::new();
    hasher.update(string.as_bytes());
    hex::encode(hasher.finalize())
}
