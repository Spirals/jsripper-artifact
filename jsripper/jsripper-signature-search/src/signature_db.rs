use std::{borrow::Cow, path::Path, time::Duration};

use log::error;
use sqlx::{
    pool::Pool,
    sqlite::{SqliteConnectOptions, SqliteJournalMode, SqlitePool, SqliteSynchronous},
};

#[derive(Debug, Clone)]
pub struct FunctionSignature<'a> {
    page_domain: &'a str,
    resource_path: Cow<'a, str>,
    resource_url: &'a str,
    resource_sha256: &'a str,
    resource_in_filter_lists: bool,
    is_first_party: bool,
    resource_byte_size: u32,
    function_byte_start: u32,
    function_body_byte_start: u32,
    function_byte_end: u32,
    function_sig: &'a str,
}

impl<'a> FunctionSignature<'a> {
    #[allow(clippy::too_many_arguments)]
    #[must_use]
    pub fn new(
        page_domain: &'a str,
        resource_path: &'a Path,
        resource_url: &'a str,
        resource_sha256: &'a str,
        resource_in_filter_lists: bool,
        is_first_party: bool,
        resource_byte_size: u32,
        function_byte_start: u32,
        function_body_byte_start: u32,
        function_byte_end: u32,
        function_sig: &'a str,
    ) -> Self {
        Self {
            page_domain,
            resource_path: resource_path.to_string_lossy(),
            resource_url,
            resource_sha256,
            resource_in_filter_lists,
            is_first_party,
            resource_byte_size,
            function_byte_start,
            function_body_byte_start,
            function_byte_end,
            function_sig,
        }
    }
}

#[derive(Debug, Clone)]
pub struct SignatureDb {
    db_pool: SqlitePool,
}

impl SignatureDb {
    pub async fn new(db_file_path: &Path) -> Result<Self, sqlx::Error> {
        // Disable durability for increased write speed: it the program crashes mid-way, the database
        // may get corrupted, but it is acceptable in this use case
        let options = SqliteConnectOptions::new()
            .filename(db_file_path)
            .create_if_missing(true)
            .busy_timeout(Duration::from_secs(60))
            .journal_mode(SqliteJournalMode::Off)
            .synchronous(SqliteSynchronous::Off);

        let db_pool = Pool::connect_with(options).await?;

        let mut db_conn = db_pool.acquire().await?;

        sqlx::query(
            r#"
CREATE TABLE IF NOT EXISTS data (
    page_domain TEXT,
    resource_url TEXT,
    resource_sha256 TEXT,
    resource_in_filter_lists INTEGER,
    is_first_party INTEGER,
    resource_byte_size INTEGER,
    resource_path TEXT,
    function_byte_start INTEGER,
    function_body_byte_start INTEGER,
    function_byte_end INTEGER,
    function_sig TEXT
    );
"#,
        )
        .execute(&mut db_conn)
        .await?;

        Ok(Self { db_pool })
    }

    pub async fn write_signature(
        &self,
        function_signature: &FunctionSignature<'_>,
    ) -> Result<(), sqlx::Error> {
        let mut q = self._write_signature(function_signature).await;

        while let Err(err) = q {
            error!("Error while writing a signature in the DB ({err}), retrying");
            q = self._write_signature(function_signature).await;
        }

        Ok(())
    }

    async fn _write_signature(
        &self,
        function_signature: &FunctionSignature<'_>,
    ) -> Result<(), sqlx::Error> {
        let mut db_conn = self.db_pool.acquire().await;

        while let Err(err) = db_conn {
            error!("Error while acquiring a database connection ({err}), retrying");
            db_conn = self.db_pool.acquire().await;
        }

        let mut db_conn = db_conn?;

        sqlx::query(
            r#"
INSERT INTO data (
    page_domain,
    resource_url,
    resource_sha256,
    resource_in_filter_lists,
    is_first_party,
    resource_byte_size,
    resource_path,
    function_byte_start,
    function_body_byte_start,
    function_byte_end,
    function_sig
    )
VALUES(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)
"#,
        )
        .bind(function_signature.page_domain)
        .bind(function_signature.resource_url)
        .bind(function_signature.resource_sha256)
        .bind(function_signature.resource_in_filter_lists)
        .bind(function_signature.is_first_party)
        .bind(function_signature.resource_byte_size)
        .bind(&function_signature.resource_path)
        .bind(function_signature.function_byte_start)
        .bind(function_signature.function_body_byte_start)
        .bind(function_signature.function_byte_end)
        .bind(function_signature.function_sig)
        .execute(&mut db_conn)
        .await?;

        Ok(())
    }
}
