#![deny(clippy::pedantic)]

use std::path::PathBuf;

use clap::Parser;
use jsripper_signature_search::generate_function_signatures;
use log::info;
use log::LevelFilter;
use miette::IntoDiagnostic;

// Change the allocator for improved memory usage efficiency
#[cfg(not(target_env = "msvc"))]
#[global_allocator]
static GLOBAL: jemallocator::Jemalloc = jemallocator::Jemalloc;

#[derive(Parser, Debug)]
#[command(version, long_about = None)]
struct Args {
    /// Data directory, see the README for details about its expected structure.
    #[arg(long)]
    data_dir: PathBuf,

    /// Path of the database to create, where the functions signatures will be stored.
    #[arg(long)]
    db_path: PathBuf,
}

fn main() -> miette::Result<()> {
    const JS_DIRECTORY_NAME: &str = "js";

    env_logger::Builder::from_default_env()
        .filter_module(module_path!(), LevelFilter::Info)
        .format_timestamp_millis()
        .init();

    let args = Args::parse();

    let config = jsripper_config::Config::read_from_file(&args.data_dir).into_diagnostic()?;

    if args.db_path.exists() {
        eprintln!("A file already exists in place of the given database file.");
        std::process::exit(1);
    }

    let js_file_entries = walkdir::WalkDir::new(&args.data_dir)
        .sort_by_file_name()
        .into_iter()
        .filter_entry(|e| {
            e.file_type().is_dir() || e.path().extension().unwrap() == JS_DIRECTORY_NAME
        });
    let js_file_paths = js_file_entries
        .filter_map(|e| {
            if e.as_ref().unwrap().file_type().is_file() {
                Some(PathBuf::from(e.unwrap().path()))
            } else {
                None
            }
        })
        .collect::<Vec<_>>();

    let filter_list_paths = config
        .filter_lists
        .iter()
        .map(|n| args.data_dir.join(n))
        .collect::<Vec<_>>();

    tokio::runtime::Builder::new_multi_thread()
        .enable_all()
        .thread_stack_size(4 * 1024 * 1024)
        .build()
        .unwrap()
        .block_on(async {
            generate_function_signatures(&js_file_paths, &filter_list_paths, &args.db_path).await;
        });

    info!("Done!");

    Ok(())
}
