<div align="center">
  <h1>JsRipper</h1>
  <p>Generating robust JavaScript function signatures</p>
</div>

## Description

JsRipper generates JavaScript function signatures robust to script minification, and provides tools
to cross-search these signatures into a large user-provided corpus of JavaScript files.
These function signatures are designed to identify 'identical' functions, based on their minified
source code as found in the wild.

See [Function signatures design](#function-signature-design) to learn more about the JavaScript
function signature design.

## Usage

You need an up-to-date Rust toolchain to compile JsRipper.
Please see the [official installation guide](https://www.rust-lang.org/tools/install).

### JsRipper Assistant

[JsRipper Assistant](./jsripper-assistant) is the CLI tool allowing you to manually classify
individual functions based on specific criteria.

The help message of the CLI program can be obtained with:

```sh
cargo run --release --features='cli' --package jsripper-assistant -- --help
```

#### Generating a signature from a single function

JsRipper Assistant can be asked to generate the function signature of a single function, by
providing the source code of that function in a text file.
For details, the help message can be obtained using the following:

```sh
cargo run --release --features='cli' --package jsripper-assistant -- generate-signature --help
```

#### Classifying functions from a corpus of scripts

The main feature of JsRipper Assistant is to help you classify functions from the JavaScript file
corpus, and generate a JsRipper filter list.
The help of the `analyze` sub-command for this classification process can be obtained with:

```sh
cargo run --release --features='cli' --package jsripper-assistant -- analyze --help
```

⚠️  The SQLite database containing the function signatures must have been generated using [JsRipper
Signature search](#jsripper-signature-search) beforehand.

#### Configuration

The criteria used by JsRipper Assistant to restrict functions to manually classify are configured
using the `jsripper.toml` [TOML](https://toml.io/) file to place in the *data directory* (see
[Expected directory structure](#expected-directory-structure)).
Documentation of the various configuration keys can be obtained using:

```sh
cargo doc --package jsripper-config --open
```

### JsRipper Signature search

[JsRipper Signature search](./jsripper-signature-search) will read every provided JavaScript file
(see [Expected directory structure](#expected-directory-structure)), compute the signatures of all
functions statements (not arrow function expressions) and store them all in a
[SQLite](https://www.sqlite.org/) database it will create at the provided file path.
This process may take quite some time depending on the number of scripts (a couple of hours for
~50 k scripts).

```sh
cargo run --release --features='cli' --package jsripper-signature-search -- --help
```

For each detected function, the following information will stored in the database:

- The domain where the function's script originates from.
- TODO

### Expected directory structure

JsRipper expects the following directory structure in the *data directory*:

```
./
├─ crawl_data/
│  ├─ example.com/
│  │  └─ js/
│  │     ├─ script0.js
│  │     └─ script1.js
│  └─ example.com/
│     └─ js/
│        ├─ script0.js
│        └─ script1.js
└─ jsripper.toml
```

The downloaded JavaScript files should be placed in a directory (here, `crawl_data`), its name is not
significant.
Each website must have a separate directory within that directory, whose name is the website's
domain.
Within that website directory, the JavaScript files must be placed in a directory named `js`.
JsRipper Signature search will consider every file in these `js` directories to be JavaScript files
and will scan them.

The configuration file [`jsripper.toml`](#configuration) must be placed at the root of the *data directory*.

## Function signature design

Given the source code of a JavaScript function, its signatures is computed using the following
steps:

1. The function source code is parsed using
  [`swc_ecma_parser`](https://docs.rs/swc_ecma_parser/latest/swc_ecma_parser/) to generate an
  [AST](https://en.wikipedia.org/wiki/Abstract_syntax_tree).
2. The AST is normalized by removing information irrelevant for the signature.
  In particular:
    - Local identifiers (function parameter names, local variable names, etc.) are removed.
    - Short identifiers, 2-character long or shorter, are removed.
    - AST [spans](https://docs.rs/swc_common/0.23.0/swc_common/struct.Span.html) are removed.
    - [Raw source fields](https://docs.rs/swc_ecma_ast/0.91.3/swc_ecma_ast/struct.Str.html#structfield.raw)
      are removed from relevant AST nodes.
3. The AST is then serialized and hashed, to produce a
  [SHA-256](https://en.wikipedia.org/wiki/SHA-2) function signature.

### JsRipper WebExtension

TODO:

- current blocking capabilities
- Webext doc (compiling, installing, using)

## License

Licensed under the MPL-2.0 license (see [LICENSE](./LICENSE)).

Copyright (c) 2023 Inria
