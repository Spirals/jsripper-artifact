use jsripper_filter_list::FilterList;
use log::Level;
use once_cell::sync::OnceCell;
use wasm_bindgen::prelude::*;
use web_sys::console;

use jsripper_function_processing::function_blocking::rip_functions_out;

#[wasm_bindgen]
pub fn rip_functions(script_url: &str, js_script: &str, filter_list_json: &str) -> String {
    console_error_panic_hook::set_once();
    console_log::init_with_level(Level::Debug).unwrap();

    static FILTER_LIST: OnceCell<FilterList> = OnceCell::new();
    let filter_list = FILTER_LIST.get_or_init(|| FilterList::from_json(filter_list_json).unwrap());

    rip_functions_out(script_url, js_script, filter_list)
}
