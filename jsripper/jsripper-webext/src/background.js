'use strict';

const { rip_functions } = wasm_bindgen;

browser.webRequest.onBeforeRequest.addListener(
  setupScriptReponseRewriting,
  { urls: ['<all_urls>'], types: ['script'] },
  ['blocking'],
);

(async () => {
  window.config = await (await fetch(browser.runtime.getURL('assets/tracking-list.json'))).text();
})();

function setupScriptReponseRewriting(details) {
  const filter = browser.webRequest.filterResponseData(details.requestId);

  let data = [];

  filter.ondata = (event) => {
    data.push(event.data);
  };

  const decoder = new TextDecoder('utf-8');
  const encoder = new TextEncoder();

  filter.onstop = async () => {
    if (data.length === 0) {
      filter.disconnect();
      return;
    }

    const totalLength = data.map((d) => d.byteLength).reduce((sum, l) => sum + l);
    let bytes = new Uint8Array(totalLength);

    // Concatenate the received chunks into a single typed array
    let runningLength = 0;
    for (const datum of data) {
      const array = new Uint8Array(datum);
      bytes.set(array, runningLength);
      runningLength += array.byteLength;
    }

    let chars = decoder.decode(bytes);

    console.log('Ripping functions', performance.now());

    const wasmUrl = browser.runtime.getURL('jsripper-wasm/pkg/jsripper_wasm_bg.wasm');
    await wasm_bindgen(wasmUrl);

    try {
      chars = rip_functions(details.url, chars, window.config);
      console.log('Functions ripped', performance.now());
      bytes = encoder.encode(chars);
      filter.write(bytes);
    } catch (err) {
      console.error(err);
    } finally {
      filter.disconnect();
    }
  }
}
