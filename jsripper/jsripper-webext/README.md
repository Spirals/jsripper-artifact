# JsRipper WebExtension

Proof-of-concept WebExtension to block function matching known function signatures.

## Function signatures

Functions are to be added to the `assets/tracking.txt` file, loaded by the WebExtension at runtime.

## Building

First, install the Rust toolchain and `wasm-pack` by following the instructions from the
[Rust Wasm Book](https://rustwasm.github.io/docs/book/game-of-life/setup.html).

Then, compile the WASM with the following:

```sh
(cd jsripper-wasm && wasm-pack build --release --no-typescript --target no-modules)
```

The WebExtension can finally be loaded in a browser.

## License

Licensed under the MPL-2.0 license (see LICENSE).

Copyright (c) 2022 Inria
