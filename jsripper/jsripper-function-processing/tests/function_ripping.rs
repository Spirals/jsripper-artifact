use std::{fs, path::Path};

use insta::{assert_snapshot, assert_yaml_snapshot};
use jsripper_filter_list::FilterList;
use jsripper_function_processing::{
    function_blocking,
    function_extraction::{FunctionPos, Script},
    function_signature,
};
use swc_ecma_ast::Function;
use test_case::test_case;

#[test_case("only_function.js", "only_function"; "only_function")]
#[test_case("script_0.js", "script_0"; "script_0")]
#[test_case("bootstrap.min.js", "bootstrap"; "bootstrap")]
fn test_function_extraction(script_name: &str, description: &str) {
    let js = get_fixture_script(script_name);
    let (_functions, function_positions) = extract_functions(&js);

    assert_yaml_snapshot!(
        format!("function_extraction_{description}"),
        function_positions
    );
}

fn extract_functions(js: &str) -> (Vec<Function>, Vec<FunctionPos>) {
    let script = Script::parse(js).unwrap();
    let functions = script.functions().unwrap();

    let function_positions = functions
        .iter()
        .map(|f| script.function_position(f))
        .collect::<Vec<_>>();

    (functions, function_positions)
}

#[test_case("only_function.js", "only_function"; "only_function")]
#[test_case("script_0.js", "script_0"; "script_0")]
fn test_function_signature_generation(script_name: &str, description: &str) {
    let js = get_fixture_script(script_name);
    let (functions, _function_positions) = extract_functions(&js);

    let signatures = functions
        .iter()
        .map(function_signature::generate_function_ast_signature)
        .collect::<Vec<_>>();

    assert_yaml_snapshot!(
        format!("function_signature_generation_{description}"),
        signatures
    );
}

#[test_case("script_0.js", "filter_list_0.json", "script_0"; "script_0")]
fn test_function_ripping(script_name: &str, filter_list_name: &str, description: &str) {
    let js = get_fixture_script(script_name);

    let filter_list = get_filter_list(filter_list_name);
    let ripped_js = function_blocking::rip_functions_out("", &js, &filter_list);

    assert_snapshot!(format!("function_ripping_{description}"), ripped_js);
}

fn get_filter_list(filter_list_name: &str) -> FilterList {
    let json_filter_list = fs::read_to_string(
        Path::new(env!("CARGO_MANIFEST_DIR"))
            .join("../tests/fixtures/filter_lists")
            .join(filter_list_name),
    )
    .unwrap();

    FilterList::from_json(&json_filter_list).unwrap()
}

fn get_fixture_script(script_name: &str) -> String {
    fs::read_to_string(
        Path::new(env!("CARGO_MANIFEST_DIR"))
            .join("../tests/fixtures/scripts")
            .join(script_name),
    )
    .unwrap()
}
