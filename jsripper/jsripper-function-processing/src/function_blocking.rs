//! Rip out functions based on a filter list

use log::debug;

use crate::{function_extraction::Script, function_signature::generate_function_ast_signature};
use jsripper_filter_list::FilterList;

/// Remove functions bodies as instructed by the given filter list.
pub fn rip_functions_out(script_url: &str, js_script: &str, filter_list: &FilterList) -> String {
    debug!("Searching functions in {script_url}");

    let script = Script::parse(js_script).unwrap();
    let functions = script.functions().unwrap();

    if script_url.contains("header-second") {
        debug!("Count of functions in {script_url}: {}", functions.len());
    }

    let function_positions_to_rip = functions.iter().filter_map(|f| {
        let sig = generate_function_ast_signature(f);
        if filter_list.matches_signature(&sig) {
            debug!("A function matched signature {sig}");
            Some(script.function_position(f))
        } else {
            None
        }
    });

    debug!(
        "{} function(s) will be ripped out in {}",
        function_positions_to_rip.clone().count(),
        script_url,
    );

    let mut ripped_js = js_script.to_string();
    let mut offset = 0;
    for function_position in function_positions_to_rip {
        ripped_js.replace_range(
            function_position.body_start_offset() + 1 - offset
                ..function_position.end_offset() - 1 - offset,
            "",
        );
        offset = function_position.end_offset() - 1 - function_position.body_start_offset();
    }

    ripped_js
}
