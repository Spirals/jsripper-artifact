//! Extract the byte positions of functions of a JavaScript file.
//!
//! ```
//! use insta::{assert_debug_snapshot, assert_yaml_snapshot};
//! use jsripper_function_processing::function_extraction::Script;
//!
//! let script = Script::parse(
//!     r#"function abc(p0, p1) {const f = function() {}; console.log('Hello world!');}
//! // Comment
//! function def(p2, p3) {
//!   console.log('Hello');
//!   const f = () => {};
//!   console.log('Hello world!');
//! }
//! "#,
//!     )
//!     .unwrap();
//!
//! let functions = script.functions().unwrap();
//!
//! // Arrow function expressions are ignored
//! assert_eq!(functions.len(), 3);
//!
//! assert_debug_snapshot!(script.function_position(&functions[0]), @r###"
//! FunctionPos {
//!     start_offset: 0,
//!     body_start_offset: 21,
//!     end_offset: 76,
//! }
//! "###);
//!
//! assert_debug_snapshot!(script.function_position(&functions[1]), @r###"
//! FunctionPos {
//!     start_offset: 32,
//!     body_start_offset: 43,
//!     end_offset: 45,
//! }
//! "###);
//! ```

use log::{debug, error};
use serde::{Deserialize, Serialize};
use swc_common::{sync::Lrc, FileName, SourceMap};
use swc_ecma_ast::{EsVersion, Expr, Function, Script as SwcScript};
use swc_ecma_parser::{
    error::Error as ParserError, lexer::Lexer, EsConfig, Parser, StringInput, Syntax,
};
use swc_ecma_visit::VisitAllWith;

struct FunctionVisitor {
    functions: Vec<Function>,
}

impl swc_ecma_visit::VisitAll for FunctionVisitor {
    fn visit_function(&mut self, n: &Function) {
        self.functions.push(n.clone());
    }
}

impl FunctionVisitor {
    fn new() -> Self {
        Self {
            functions: Vec::new(),
        }
    }

    fn into_functions(self) -> Vec<Function> {
        self.functions
    }
}

/// Represents the byte position of a function within a script file.
#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct FunctionPos {
    start_offset: usize,
    body_start_offset: usize,
    end_offset: usize,
}

impl FunctionPos {
    fn from_offsets(
        start_offset: usize,
        body_start_offset: usize,
        end_offset: usize,
    ) -> Option<Self> {
        if body_start_offset < start_offset {
            return None;
        }

        if end_offset < body_start_offset + 1 {
            return None;
        }

        Some(Self {
            // name,
            start_offset,
            body_start_offset,
            end_offset,
        })
    }

    /// Get the byte offset of the start of the function.
    #[must_use]
    pub fn start_offset(&self) -> usize {
        self.start_offset
    }

    /// Get the byte offset of the end of the function prototype.
    #[must_use]
    #[inline]
    pub fn body_start_offset(&self) -> usize {
        self.body_start_offset
    }

    /// Get the byte offset of the end of the function.
    #[must_use]
    pub fn end_offset(&self) -> usize {
        self.end_offset
    }
}

/// Parse a JavaScript file and extract the functions defined with the `function` keyword (arrow
/// functions expressions are ignored).
#[derive(Clone)]
pub struct Script {
    cm: Lrc<SourceMap>,
    script: SwcScript,
}

impl Script {
    /// Parse the given JavaScript file.
    pub fn parse(js_script: &str) -> Result<Self, ParserError> {
        let cm: Lrc<SourceMap> = Default::default();

        let fm = cm.new_source_file(FileName::Custom("test.js".into()), js_script.to_string());
        let lexer = Lexer::new(
            Syntax::Es(EsConfig::default()),
            EsVersion::Es2022,
            StringInput::from(&*fm),
            None,
        );

        let mut parser = Parser::new_from(lexer);

        for e in parser.take_errors() {
            error!("Error while parsing script: {:?}", e);
        }

        Ok(Self {
            cm,
            script: parser.parse_script()?,
        })
    }

    /// Get the extracted functions.
    /// Only functions defined using the `function` keyword are extracted (arrow function
    /// expressions are ignored).
    pub fn functions(&self) -> Result<Vec<Function>, ParserError> {
        debug!("Visiting functions");
        let mut function_visitor = FunctionVisitor::new();
        self.script.visit_all_with(&mut function_visitor);
        let functions = function_visitor.into_functions();
        debug!("Visited functions");

        Ok(functions)
    }

    /// Get the function byte position within the original script file.
    pub fn function_position(&self, function: &Function) -> FunctionPos {
        // Span's BytePos are 1-based (0 is reserved)
        let start_offset =
            usize::try_from(self.cm.lookup_byte_offset(function.span.lo).pos.0).unwrap();
        let end_offset =
            usize::try_from(self.cm.lookup_byte_offset(function.span.hi).pos.0).unwrap();
        let body_start_offset = usize::try_from(
            self.cm
                .lookup_byte_offset(function.body.as_ref().unwrap().span.lo)
                .pos
                .0,
        )
        .unwrap();

        FunctionPos::from_offsets(start_offset, body_start_offset, end_offset).unwrap()
    }
}

pub(crate) fn parse_expr(js_function: &str) -> Result<Box<Expr>, ParserError> {
    let cm: Lrc<SourceMap> = Default::default();

    let fm = cm.new_source_file(
        FileName::Custom("test.js".into()),
        js_function.to_string(),
    );
    let lexer = Lexer::new(
        Syntax::Es(EsConfig::default()),
        EsVersion::Es2022,
        StringInput::from(&*fm),
        None,
    );

    let mut parser = Parser::new_from(lexer);

    for e in parser.take_errors() {
        error!("Error while parsing script: {:?}", e);
    }

    parser.parse_expr()
}
