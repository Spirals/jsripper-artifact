//! Generate a function signature to identify it in other scripts.

use log::trace;
use sha2::Digest;
use swc_atoms::JsWord;
use swc_common::{source_map::Span, BytePos};
use swc_ecma_ast::{Expr, FnDecl, FnExpr, Function, Ident, MemberExpr, Param, VarDeclarator};
use swc_ecma_visit::{VisitMutWith, VisitWith};

fn dummy_ident() -> Ident {
    Ident::new("".into(), Span::default())
}

struct SpanRemoverVisitor;

impl swc_ecma_visit::VisitMut for SpanRemoverVisitor {
    fn visit_mut_span(&mut self, n: &mut Span) {
        *n = Span::default();
    }
}

struct RawLiteralRemoverVisitor;

impl swc_ecma_visit::VisitMut for RawLiteralRemoverVisitor {
    // Remove the raw literals of strings, otherwise double quoted strings are not understood as
    // being the same as single quoted ones
    fn visit_mut_str(&mut self, n: &mut swc_ecma_ast::Str) {
        n.raw = None;
    }
}

struct IdentifierLocatorVisitor {
    identifiers_to_remove: Vec<JsWord>,
}

impl swc_ecma_visit::Visit for IdentifierLocatorVisitor {
    fn visit_ident(&mut self, n: &Ident) {
        self.identifiers_to_remove.push(n.sym.clone());
    }
}
impl IdentifierLocatorVisitor {
    fn new() -> Self {
        Self {
            identifiers_to_remove: Vec::new(),
        }
    }
}

struct VariableLocatorVisitor {
    identifiers_to_remove: Vec<JsWord>,
}

// Identify the identifiers we want to remove from the signature
impl swc_ecma_visit::Visit for VariableLocatorVisitor {
    fn visit_var_declarator(&mut self, n: &VarDeclarator) {
        let mut sub_visitor = IdentifierLocatorVisitor::new();
        n.name.visit_with(&mut sub_visitor);
        self.identifiers_to_remove
            .extend(sub_visitor.identifiers_to_remove);
    }

    fn visit_param(&mut self, n: &Param) {
        let mut sub_visitor = IdentifierLocatorVisitor::new();
        n.visit_with(&mut sub_visitor);
        self.identifiers_to_remove
            .extend(sub_visitor.identifiers_to_remove);
    }

    fn visit_member_expr(&mut self, n: &MemberExpr) {
        if let Some(ident) = n.prop.as_ident() {
            if ident.sym == *"exports" {
                if let Expr::Ident(ref obj) = *n.obj {
                    // Mark the `r` for removal in `r.exports` because this variable comes from outside
                    // the function and is also name mangled
                    self.identifiers_to_remove.push(obj.sym.clone());
                }
            }
        }
    }
}

impl VariableLocatorVisitor {
    fn new() -> Self {
        Self {
            identifiers_to_remove: Vec::new(),
        }
    }
    fn get_identifier_atoms_to_remove(&self) -> &[JsWord] {
        &self.identifiers_to_remove
    }
}

struct ShortIdentifierRemoverVisitor;

impl swc_ecma_visit::VisitMut for ShortIdentifierRemoverVisitor {
    fn visit_mut_ident(&mut self, n: &mut Ident) {
        // If the identifier is 2-character long or less
        if n.span.hi - n.span.lo <= BytePos(3) {
            *n = dummy_ident();
        }
    }
}

struct FunctionNameRemoverVisitor;

impl swc_ecma_visit::VisitMut for FunctionNameRemoverVisitor {
    fn visit_mut_fn_decl(&mut self, n: &mut FnDecl) {
        n.ident = dummy_ident();
    }

    fn visit_mut_fn_expr(&mut self, n: &mut FnExpr) {
        n.ident = Some(dummy_ident());
    }
}

struct ParamRemoverVisitor;

impl swc_ecma_visit::VisitMut for ParamRemoverVisitor {
    fn visit_mut_params(&mut self, n: &mut Vec<Param>) {
        *n = Vec::new();
    }
}

struct IdentRemoverVisitor {
    identifiers_to_remove: Vec<JsWord>,
}
impl swc_ecma_visit::VisitMut for IdentRemoverVisitor {
    fn visit_mut_ident(&mut self, n: &mut Ident) {
        if self.identifiers_to_remove.iter().any(|a| a[..] == n.sym) {
            *n = dummy_ident();
        }
    }
}
impl IdentRemoverVisitor {
    fn new(identifiers_to_remove: Vec<JsWord>) -> Self {
        Self {
            identifiers_to_remove,
        }
    }
}

/// Generate the SHA-256 AST function signature of the given function.
///
/// The function signature is computed on its AST where the local identifiers (e.g., local
/// variables) are erased.
///
/// Given a JavaScript file, [`Function`]s can be obtained using
/// [`Script::parse()`](crate::function_extraction::Script::parse) and
/// [`Script::functions()`](crate::function_extraction::Script::functions).
#[inline]
#[must_use]
pub fn generate_function_ast_signature(function: &Function) -> String {
    let function = normalize_function_ast(function);

    let module_json = serde_json::to_string(&function).unwrap();

    let mut hasher = sha2::Sha256::new();
    hasher.update(module_json.as_bytes());
    hex::encode(hasher.finalize())
}

pub(crate) fn normalize_function_ast(function: &Function) -> Function {
    let mut function = function.clone();

    // Remove the short identifiers
    function.visit_mut_with(&mut ShortIdentifierRemoverVisitor);

    // Remove spans in-place
    function.visit_mut_with(&mut SpanRemoverVisitor);

    // Remove raw literals in-place
    function.visit_mut_with(&mut RawLiteralRemoverVisitor);

    // Remove the function names
    function.visit_mut_with(&mut FunctionNameRemoverVisitor);

    // Remove the function parameters
    function.visit_mut_with(&mut ParamRemoverVisitor);

    // Identify the identifiers to remove
    let mut variable_identifier_remover_visitor = VariableLocatorVisitor::new();
    function.visit_with(&mut variable_identifier_remover_visitor);
    let identifiers_to_remove =
        variable_identifier_remover_visitor.get_identifier_atoms_to_remove();
    trace!("Identifiers to remove: {identifiers_to_remove:?}");

    // Remove the identifiers
    function.visit_mut_with(&mut IdentRemoverVisitor::new(
        identifiers_to_remove.to_vec(),
    ));

    function
}

#[cfg(test)]
mod tests {
    use super::*;

    use swc_ecma_ast::Expr;

    fn parse_fn_expr(expr_str: &str) -> Function {
        let expr = crate::function_extraction::parse_expr(expr_str).unwrap();
        if let Expr::Fn(fn_expr) = *expr {
            *fn_expr.function
        } else {
            unimplemented!();
        }
    }

    #[test]
    fn test_removed_spans() {
        let sig0 = generate_function_ast_signature(&parse_fn_expr(
            r#"
function test(p0, p1) {
    const abc = 2;
    console.log(abc);
}
        "#,
        ));

        let sig1 = generate_function_ast_signature(&parse_fn_expr(
            r#"
function test(p0, p1) {
    const b = 2;
    console.log(b);
}
        "#,
        ));

        assert_eq!(sig0, sig1);
    }

    #[test]
    fn test_removed_string_raw_literals() {
        let sig0 = generate_function_ast_signature(&parse_fn_expr(
            r#"
function test() {
    console.log('abc');
}
        "#,
        ));

        let sig1 = generate_function_ast_signature(&parse_fn_expr(
            r#"
function test() {
    console.log("abc");
}
        "#,
        ));

        assert_eq!(sig0, sig1);
    }

    #[test]
    fn test_whitespace() {
        let sig0 = generate_function_ast_signature(&parse_fn_expr(
            r#"
function test(p0, p1) {
    const abc = 2;
    console.log(abc);
}
        "#,
        ));

        let sig1 = generate_function_ast_signature(&parse_fn_expr(
            r#"function test(p0, p1) { const b = 2; console.log(b); }"#,
        ));

        assert_eq!(sig0, sig1);
    }

    #[test]
    fn test_variable_var_hoisting() {
        let sig0 = generate_function_ast_signature(&parse_fn_expr(
            r#"
function test() {
    console.log(abc);
    var abc = 2;
}
        "#,
        ));

        let sig1 = generate_function_ast_signature(&parse_fn_expr(
            r#"
function test() {
    console.log(def);
    var def = 2;
}
        "#,
        ));

        assert_eq!(sig0, sig1);
        assert_eq!(
            sig0,
            "8031ba8699a0eec3cf4b48018f9ca8edc103cbef6d4d00eb1d6bdc5c4bff14b1",
        );
    }

    #[test]
    fn test_params() {
        let sig0 = generate_function_ast_signature(&parse_fn_expr(
            r#"
function test(p0, p1) {
    const a = 2;
    console.log(a);
}
        "#,
        ));

        let sig1 = generate_function_ast_signature(&parse_fn_expr(
            r#"
function test(p0, p1, p2) {
    const b = 2;
    console.log(b);
}
        "#,
        ));

        assert_eq!(sig0, sig1);
    }

    #[test]
    fn test_anonymous_function() {
        let sig0 = generate_function_ast_signature(&parse_fn_expr(
            r#"
function() {
    const a = 2;
    console.log(a);
}
        "#,
        ));

        let sig1 = generate_function_ast_signature(&parse_fn_expr(
            r#"
function() {
    const b = 2;
    console.log(b);
}
        "#,
        ));

        assert_eq!(sig0, sig1);
    }

    #[test]
    fn test_for_loop_binding() {
        let sig0 = generate_function_ast_signature(&parse_fn_expr(
            r#"
function() {
    for (const i of [0, 1, 2]) {
        console.log(i);
    }
}
        "#,
        ));

        let sig1 = generate_function_ast_signature(&parse_fn_expr(
            r#"
function() {
    for (const j of [0, 1, 2]) {
        console.log(j);
    }
}
        "#,
        ));

        assert_eq!(sig0, sig1);
    }

    #[test]
    fn test_function_name() {
        let sig0 = generate_function_ast_signature(&parse_fn_expr(
            r#"
function test0() {
    const a = 2;
    console.log(a);
}

test0.foo = 42;
        "#,
        ));

        let sig1 = generate_function_ast_signature(&parse_fn_expr(
            r#"
function test1() {
    const b = 2;
    console.log(b);
}

test1.foo = 42;
        "#,
        ));

        assert_eq!(sig0, sig1);
    }

    #[test]
    #[ignore]
    fn test_method_name_same_as_var() {
        let sig0 = generate_function_ast_signature(&parse_fn_expr(
            r#"
function test0() {
    const a = 2;
    console.log(a);
    console.log(window.a);
}
        "#,
        ));

        let sig1 = generate_function_ast_signature(&parse_fn_expr(
            r#"
function test1() {
    const b = 2;
    console.log(b);
    console.log(window.a);
}
        "#,
        ));

        assert_eq!(sig0, sig1);
    }

    #[test]
    fn test_closed_on_variable() {
        let sig0 = generate_function_ast_signature(&parse_fn_expr(
            r#"
function test() {
    const a = 2;
    console.log(a);
    console.log(c);
}
        "#,
        ));

        let sig1 = generate_function_ast_signature(&parse_fn_expr(
            r#"
function test() {
    const b = 2;
    console.log(b);
    console.log(c);
}
        "#,
        ));

        assert_eq!(sig0, sig1);
    }

    #[test]
    fn test_commonjs_module_var() {
        let sig0 = generate_function_ast_signature(&parse_fn_expr(
            r#"
function test() {
    const a = 2;
    console.log(a);
    r.exports = {};
}
        "#,
        ));

        let sig1 = generate_function_ast_signature(&parse_fn_expr(
            r#"
function test() {
    const b = 2;
    console.log(b);
    module.exports = {};
}
        "#,
        ));

        assert_eq!(sig0, sig1);

        let sig0 = generate_function_ast_signature(&parse_fn_expr(
            r#"
function test() {
    const a = 2;
    console.log(a);
    r.other = {};
}
        "#,
        ));

        let sig1 = generate_function_ast_signature(&parse_fn_expr(
            r#"
function test() {
    const b = 2;
    console.log(b);
    module.other = {};
}
        "#,
        ));

        assert_ne!(sig0, sig1);
    }
}
