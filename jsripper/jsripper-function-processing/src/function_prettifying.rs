//! Prettify a JavaScript function source code.

use std::io;

use swc_common::{source_map::Span, sync::Lrc, SourceMap};
use swc_ecma_ast::{Expr, ExprStmt, FnExpr, Script as SwcScript, Stmt};
use swc_ecma_codegen::{text_writer::JsWriter, Emitter};
use swc_ecma_parser::error::Error as ParserError;

use crate::function_extraction::parse_expr;
use crate::function_signature;

/// Represents possible errors during JavaScript function prettifying.
#[derive(Debug)]
pub enum Error {
    Io(io::Error),
    /// The given source code could not be parsed.
    ParserError(ParserError),
}

impl From<io::Error> for Error {
    fn from(err: io::Error) -> Self {
        Self::Io(err)
    }
}

impl From<ParserError> for Error {
    fn from(err: ParserError) -> Self {
        Self::ParserError(err)
    }
}

/// Prettify the source code of the given JavaScript function.
pub fn prettify_js_function(js_expr: &str) -> Result<String, Error> {
    let function_expr = parse_expr(js_expr)?;
    Ok(print_expr(*function_expr)?)
}

/// Normalize the function AST and prettify the result.
pub fn normalize_function_ast(js_expr: &str) -> Result<String, Error> {
    let function_expr = parse_expr(js_expr)?;
    let Expr::Fn(FnExpr{function,..}) = *function_expr else {
        panic!("Expression was not a function when normalizing function AST for printing")
    };
    let normalized_function_expr = function_signature::normalize_function_ast(&function);
    Ok(print_expr(normalized_function_expr.into())?)
}

fn print_expr(expr: Expr) -> Result<String, io::Error> {
    let cm: Lrc<SourceMap> = Default::default();

    let mut buf = vec![];

    {
        let mut emitter = Emitter {
            cfg: swc_ecma_codegen::Config {
                ..Default::default()
            },
            cm: cm.clone(),
            comments: None,
            wr: JsWriter::new(cm, "\n", &mut buf, None),
        };

        emitter.emit_script(&SwcScript {
            span: Span::default(),
            body: vec![Stmt::Expr(ExprStmt {
                span: Span::default(),
                expr: Box::new(expr),
            })],
            shebang: None,
        })?;
    }

    Ok(String::from_utf8_lossy(&buf).to_string())
}
