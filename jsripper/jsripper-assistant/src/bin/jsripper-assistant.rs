#![deny(clippy::pedantic)]

use std::path::PathBuf;

use clap::{Args, Parser, Subcommand};
use jsripper_assistant::{generate_signature_from_file, JsRipperAssistant};
use log::LevelFilter;
use miette::IntoDiagnostic;

#[derive(Parser)]
#[clap(version)]
struct Cli {
    #[command(subcommand)]
    command: Commands,
}

#[derive(Subcommand)]
enum Commands {
    /// Analyze data collected from a crawl.
    Analyze(AnalyzeArgs),
    /// Generate the signature of a given function.
    GenerateSignature(GenerateSignatureArgs),
}

#[derive(Args)]
struct AnalyzeArgs {
    /// Data directory, see the README for details about its expected structure.
    #[arg(long)]
    data_dir: PathBuf,

    /// Path of the database where the functions signatures can be found.
    #[arg(long)]
    db_path: PathBuf,

    /// Domain of which to restrict classifying functions.
    #[arg(long)]
    page_domain: Option<String>,

    /// Which set of configured regexes to use.
    #[arg(long)]
    search_regex_set: String,

    /// Path of the filter list file to generate, existing rules will be discarded.
    #[arg(long)]
    filter_list_path: PathBuf,
}

#[derive(Args)]
struct GenerateSignatureArgs {
    /// Path of the JavaScript containing *only* the function of which to generate a signature.
    path: PathBuf,
    /// Path of the filter list file to generate, will be overwritten if it already exists.
    filter_list_path: PathBuf,
}

#[tokio::main]
async fn main() -> miette::Result<()> {
    env_logger::Builder::from_default_env()
        .filter_module(module_path!(), LevelFilter::Info)
        .format_timestamp_millis()
        .init();

    let cli = Cli::parse();

    match &cli.command {
        Commands::Analyze(args) => {
            let config =
                jsripper_config::Config::read_from_file(&args.data_dir).into_diagnostic()?;

            let search_regexes = config
                .candidate_function_regexes
                .get(&args.search_regex_set)
                .expect("Given regex set was not found in the configuration file");

            let assistant = JsRipperAssistant::new(
                &args.db_path,
                &args.data_dir,
                &args.search_regex_set,
                &search_regexes,
                &config.allowlist,
                &args.filter_list_path,
            )
            .await
            .into_diagnostic()?;
            assistant
                .extract_candidate_functions_and_prompt(args.page_domain.as_deref())
                .await
                .into_diagnostic()?;
        }
        Commands::GenerateSignature(args) => {
            let function_signature =
                generate_signature_from_file(&args.path, &args.filter_list_path);
            println!("Function signature {}", function_signature);
        }
    }

    Ok(())
}
