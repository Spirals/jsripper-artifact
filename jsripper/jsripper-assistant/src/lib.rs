//! Assistant to label function origin scripts, based on their source code.

// #![deny(clippy::pedantic)]

use std::{
    fs::{self, File},
    io::{self, Read, Write},
    path::{Path, PathBuf},
    pin::Pin,
    time::{Duration, SystemTime},
};

use futures::{Stream, TryStreamExt};
use jsripper_filter_list::{ByteRangeRule, FilterList, FilterListFile, FilterRule, SignatureRule};
use jsripper_function_processing::{
    function_extraction::Script, function_prettifying, function_signature,
};
use log::{debug, error, info, warn};
use regex::RegexSet;
use sqlx::{
    sqlite::{SqliteConnectOptions, SqliteConnection, SqliteJournalMode, SqliteRow},
    Pool, Row, Sqlite,
};
use tokio::sync::Mutex;
use url::Url;

use jsripper_config::AllowlistConfig;

#[derive(Debug, Clone, PartialEq, sqlx::FromRow)]
struct FunctionRow {
    page_domain: String,
    bundle_resource_url: String,
    bundle_resource_sha256: String,
    resource_sha256: String,
    resource_path: String,
    function_byte_start: u32,
    function_body_byte_start: u32,
    function_byte_end: u32,
    function_sig: String,
}

#[derive(Debug, Clone, PartialEq, sqlx::FromRow)]
struct CandidateFunctionRow {
    page_domain: String,
    in_filter_list_resource_url: String,
    bundle_resource_url: String,
    bundle_resource_sha256: String,
    resource_sha256: String,
    resource_path: String,
    function_byte_start: u32,
    function_body_byte_start: u32,
    function_byte_end: u32,
    function_sig: String,
}

pub fn generate_signature_from_file(path: &Path, filter_list_path: &Path) -> String {
    let function_string = fs::read_to_string(path).unwrap();
    let script = Script::parse(&function_string).unwrap();
    let functions = script.functions().unwrap();
    let function = functions.get(0).unwrap();
    let signature = function_signature::generate_function_ast_signature(function);

    let filter_list = FilterList::new(
        "Tracking",
        "",
        &SystemTime::now(),
        &SystemTime::now()
            .checked_add(Duration::from_secs(3600 * 24 * 365))
            .unwrap(),
    );
    let mut filter_list_file = FilterListFile::new(filter_list, filter_list_path);

    let signature_filter_rule = FilterRule::FunctionSignature(SignatureRule {
        signature: signature.clone(),
        allow: Vec::new(),
        block: Vec::new(),
    });
    filter_list_file.append_rule(signature_filter_rule);
    filter_list_file.write().unwrap();

    signature
}

#[allow(clippy::doc_markdown)]
/// The JsRipper Assistant.
#[derive(Debug)]
pub struct JsRipperAssistant {
    db_pool: Pool<Sqlite>,
    data_dir: PathBuf,
    search_regex_set: String,
    allowlist: AllowlistConfig,
    candidate_function_regexset: RegexSet,
    source_allowlist_regexset: RegexSet,
    filter_list_file: Mutex<FilterListFile>,
}

impl JsRipperAssistant {
    #[allow(clippy::doc_markdown)]
    /// Create a new JsRipper assistant.
    ///
    /// # Errors
    /// Returns a [`sqlx::Error`] when an error happens when interacting with the function
    /// signatures database.
    ///
    /// # Panics
    /// Panics if the [`RegexSet`] cannot be created successfully from the configured regexes.
    pub async fn new(
        db_path: &Path,
        data_dir: &Path,
        search_regex_set: &str,
        search_regexes: &[String],
        allowlist: &AllowlistConfig,
        filter_list_path: &Path,
    ) -> Result<Self, sqlx::Error> {
        let db_options = SqliteConnectOptions::new()
            .filename(db_path)
            .journal_mode(SqliteJournalMode::Wal);
        let db_pool = Pool::connect_with(db_options).await?;

        let candidate_function_regexset = RegexSet::new(search_regexes).unwrap();
        let source_allowlist_regexset = RegexSet::new(&allowlist.source_regexes).unwrap();

        let filter_list = FilterList::new(
            "Tracking",
            "",
            &SystemTime::now(),
            &SystemTime::now()
                .checked_add(Duration::from_secs(3600 * 24 * 365))
                .unwrap(),
        );
        let filter_list_file = Mutex::new(FilterListFile::new(filter_list, filter_list_path));

        Ok(Self {
            db_pool,
            data_dir: data_dir.to_path_buf(),
            search_regex_set: search_regex_set.to_owned(),
            allowlist: allowlist.clone(),
            candidate_function_regexset,
            source_allowlist_regexset,
            filter_list_file,
        })
    }

    #[allow(clippy::doc_markdown)]
    /// Extract candidate functions source code from the function signatures database and the
    /// source code files, and prompt the user to classify each one.
    ///
    /// # Errors
    /// Returns a [`sqlx::Error`] when an error happens when interacting with the function
    /// signature database.
    pub async fn extract_candidate_functions_and_prompt(
        &self,
        page_domain: Option<&str>,
    ) -> Result<(), sqlx::Error> {
        info!("Extracting candidate functions…");
        let candidate_functions = self.extract_candidate_functions().await?;
        info!("{} candidate functions found", candidate_functions.len());

        self.add_origin_label_column().await?;

        self.promt_user_to_label_functions(&candidate_functions, page_domain)
            .await?;

        Ok(())
    }

    async fn extract_candidate_functions(
        &self,
    ) -> Result<Vec<(CandidateFunctionRow, String)>, sqlx::Error> {
        info!(
            "Querying functions candidate for marking ({})…",
            self.search_regex_set,
        );
        let mut conn = self.db_pool.acquire().await?;
        // let _ = Self::query_functions_candidate_for_blocking(&mut conn);
        let mut candidate_function_rows = Self::query_functions_candidate_for_blocking(&mut conn);
        // let mut candidate_function_rows = Self::query_large_enough_functions(&mut conn);

        let allowlist_function_signatures = self
            .query_allowlist_functions(&self.allowlist.url_patterns)
            .await?;

        let mut candidate_functions = Vec::new();

        info!("Filtering out functions");
        while let Some(function_row) = candidate_function_rows.try_next().await? {
            // If this exact function originates from a script in the allowlist
            if allowlist_function_signatures.contains(&function_row.function_sig) {
                continue;
            }

            let mut bytes = Vec::new();
            let script_path = self.data_dir.join(&function_row.resource_path);
            debug!(
                "Reading script byte range {}..{} from `{}`",
                function_row.function_byte_start,
                function_row.function_byte_end,
                script_path.display(),
            );

            File::open(&script_path)
                .unwrap()
                .read_to_end(&mut bytes)
                .unwrap();

            let byte_range = &bytes[usize::try_from(function_row.function_byte_start).unwrap()
                ..usize::try_from(function_row.function_byte_end).unwrap()];

            // Read the function source code from the source file
            if let Ok(function_string) = String::from_utf8(byte_range.to_vec()) {
                if self.source_allowlist_regexset.is_match(&function_string) {
                    continue;
                }

                if self.candidate_function_regexset.is_match(&function_string) {
                    candidate_functions.push((function_row, function_string));
                }
            } else {
                error!(
                    "Error while reading a function in `{}`",
                    script_path.display(),
                );
            }
        }

        Ok(candidate_functions)
    }

    async fn promt_user_to_label_functions(
        &self,
        candidate_functions: &[(CandidateFunctionRow, String)],
        page_domain: Option<&str>,
    ) -> Result<(), sqlx::Error> {
        let functions_to_prompt = if let Some(page_domain) = page_domain {
            candidate_functions
                .iter()
                .filter(|f| f.0.page_domain == page_domain)
                .collect::<Vec<_>>()
        } else {
            candidate_functions.iter().collect::<Vec<_>>()
        };

        for (i, (function_row, function_string)) in functions_to_prompt.iter().enumerate() {
            let mut conn = self.db_pool.acquire().await?;
            let function_occurrences =
                Self::query_function_occurrences(&mut conn, &function_row.function_sig).await?;
            let function_occurrence_urls = function_occurrences
                .iter()
                .map(|f| f.bundle_resource_url.clone())
                .collect::<Vec<_>>();

            let should_mark = Self::prompt_user_whether_mark_origin(
                function_string,
                &function_row.function_sig,
                &function_row.in_filter_list_resource_url,
                &function_occurrence_urls,
                i,
                candidate_functions.len(),
            );

            if should_mark {
                self.mark_function_origin(function_row).await?;
                println!("Marked the function origin");

                self.write_filter_rules(function_row, &function_occurrences)
                    .await;
            }
        }

        Ok(())
    }

    async fn write_filter_rules(
        &self,
        candidate_function_row: &CandidateFunctionRow,
        function_occurrences: &[FunctionRow],
    ) {
        let signature_filter_rule = FilterRule::FunctionSignature(SignatureRule {
            signature: candidate_function_row.function_sig.clone(),
            allow: Vec::new(),
            block: Vec::new(),
        });
        info!(
            "Signature filter rule as JSON: {}",
            serde_json::to_string(&signature_filter_rule).unwrap(),
        );
        {
            self.filter_list_file
                .lock()
                .await
                .append_rule(signature_filter_rule);
        }

        for function_occurrence in function_occurrences {
            let byte_ranges = vec![
                function_occurrence.function_body_byte_start..function_occurrence.function_byte_end,
            ];
            let byte_range_filter_rule = FilterRule::ByteRange(ByteRangeRule {
                url: Url::parse(&function_occurrence.bundle_resource_url).unwrap(),
                sha256: function_occurrence.resource_sha256.clone(),
                byte_ranges,
            });
            info!(
                "Byte range filter rule as JSON: {}",
                serde_json::to_string(&byte_range_filter_rule).unwrap(),
            );
            self.filter_list_file
                .lock()
                .await
                .append_rule(byte_range_filter_rule);
        }

        self.filter_list_file.lock().await.write().unwrap();
    }

    async fn add_origin_label_column(&self) -> Result<(), sqlx::Error> {
        let mut conn = self.db_pool.acquire().await?;
        let add_result = sqlx::query(
            r#"
ALTER TABLE data
ADD COLUMN origin_script TEXT DEFAULT '{}';
        "#,
        )
        .execute(&mut conn)
        .await;

        // An error would happen if the column already exists
        if let Err(err) = add_result {
            warn!("Error when adding new column: {err}");
        }

        Ok(())
    }

    fn query_functions_candidate_for_blocking(
        conn: &mut SqliteConnection,
    ) -> Pin<Box<dyn Stream<Item = Result<CandidateFunctionRow, sqlx::Error>> + '_>> {
        sqlx::query_as::<_, CandidateFunctionRow>(
            r#"
CREATE INDEX IF NOT EXISTS idx_function_sig
ON data (function_sig);

CREATE TABLE IF NOT EXISTS candidate_tracking_functions
AS
SELECT
    bundle.page_domain,
    data.resource_url AS in_filter_list_resource_url,
    bundle.resource_url AS bundle_resource_url,
    bundle.resource_sha256 AS bundle_resource_sha256,
    data.resource_sha256,
    resource_path,
    function_byte_start,
    function_body_byte_start,
    function_byte_end,
    function_sig,
    COUNT(function_sig) AS cnt
FROM data
INNER JOIN (
    SELECT DISTINCT page_domain, resource_url, resource_sha256, function_sig
    FROM data
    WHERE resource_in_filter_lists = 0
) AS bundle
USING (function_sig)
WHERE resource_in_filter_lists = 1
    AND data.resource_url <> bundle.resource_url
GROUP BY function_sig
ORDER BY cnt DESC;

SELECT * FROM candidate_tracking_functions;
"#,
        )
        .fetch(conn)
    }

    fn query_large_enough_functions(
        conn: &mut SqliteConnection,
    ) -> Pin<Box<dyn Stream<Item = Result<CandidateFunctionRow, sqlx::Error>> + '_>> {
        sqlx::query_as::<_, CandidateFunctionRow>(
            r#"
SELECT * FROM candidate_tracking_functions
WHERE function_byte_end - function_body_byte_start > 1000
--GROUP BY in_filter_list_resource_url
ORDER BY cnt DESC
"#,
        )
        .fetch(conn)
    }

    async fn query_function_occurrences<'a, 'b>(
        conn: &mut SqliteConnection,
        function_sig: &str,
    ) -> Result<Vec<FunctionRow>, sqlx::Error> {
        sqlx::query_as::<_, FunctionRow>(
            r#"
SELECT
    resource_url,
    page_domain,
    resource_url AS bundle_resource_url,
    resource_sha256 AS bundle_resource_sha256,
    resource_sha256,
    resource_path,
    function_byte_start,
    function_body_byte_start,
    function_byte_end,
    function_sig
FROM data
WHERE function_sig = ?
"#,
        )
        .bind(function_sig)
        .fetch_all(conn)
        .await
    }

    async fn query_allowlist_functions(
        &self,
        allowlist_url_patterns: &[String],
    ) -> Result<Vec<String>, sqlx::Error> {
        let mut conn = self.db_pool.acquire().await?;

        let mut allowlist_functions = Vec::new();

        for allowlist_url_pattern in allowlist_url_patterns {
            allowlist_functions.append(
                &mut sqlx::query(
                    r#"
SELECT function_sig
FROM data
WHERE resource_url LIKE ?
"#,
                )
                .bind(allowlist_url_pattern)
                .map(|row: SqliteRow| row.get("function_sig"))
                .fetch_all(&mut conn)
                .await
                .unwrap(),
            );
        }

        Ok(allowlist_functions)
    }

    async fn mark_function_origin(
        &self,
        function_row: &CandidateFunctionRow,
    ) -> Result<(), sqlx::Error> {
        let mut conn = self.db_pool.acquire().await?;
        sqlx::query(
            r#"
BEGIN TRANSACTION;

UPDATE data
SET origin_script = json_insert(origin_script, '$.' || ?1, json_array())
WHERE function_sig = ?3;

UPDATE data
SET origin_script = json_insert(origin_script, '$.' || ?1 || '[#]', ?2)
WHERE function_sig = ?3;

COMMIT;
"#,
        )
        .bind(&function_row.in_filter_list_resource_url)
        .bind(&function_row.function_sig)
        .execute(&mut conn)
        .await
        .unwrap();

        Ok(())
    }

    fn prompt_user_whether_mark_origin(
        function_string: &str,
        function_sig: &str,
        in_filter_list_resource_url: &str,
        function_occurrence_urls: &[String],
        function_index: usize,
        candidate_function_count: usize,
    ) -> bool {
        let function_occurrence_count = function_occurrence_urls.len();
        let mut unique_function_occurrence_urls = function_occurrence_urls.to_vec();
        unique_function_occurrence_urls.sort_unstable();
        let url_count = dedup_with_count(&mut unique_function_occurrence_urls);
        let unique_function_occurrence_urls = unique_function_occurrence_urls
            .iter()
            .zip(url_count)
            .map(|(url, count)| format!("[×\u{a0}{count}]\t{url}"))
            .collect::<Vec<_>>()
            .join("\n");

        println!(
            "{}
[{}/{candidate_function_count}] Function ({function_sig}) from {in_filter_list_resource_url} found in the above {function_occurrence_count}\u{a0}scripts
```
{}
```",
            unique_function_occurrence_urls,
            function_index + 1,
            match function_prettifying::prettify_js_function(function_string) {
                Ok(prettified_source) => prettified_source,
                Err(err) => {
                    error!("Error while prettifying function: {err:?}");
                    function_string.to_string()
                }
            },
        );

        let question = "Should this function be marked and is unique-looking enough to avoid false positives? [y/m/N] ";

        let user_said_should_block = loop {
            match ask_question_to_user(question).as_deref() {
                Some("y") => break true,
                Some("m") => {
                    println!(
                        "{}",
                        function_prettifying::normalize_function_ast(function_string).unwrap(),
                    );
                }
                _ => break false,
            }
        };

        user_said_should_block
    }
}

fn ask_question_to_user(question: &str) -> Option<String> {
    print!("{question}");
    io::stdout().flush().unwrap();

    let mut input = String::new();
    match io::stdin().read_line(&mut input) {
        Ok(_) => {
            println!();
            return Some(input.trim_end().to_owned());
        }
        Err(error) => println!("error: {error}"),
    }

    None
}

fn dedup_with_count<T: PartialEq>(vec: &mut Vec<T>) -> Vec<usize> {
    let mut duplicate_counts = Vec::new();

    let mut last_item = None;
    for item in vec.iter() {
        if let Some(last) = last_item {
            if item == last {
                // This unwrap cannot panic because duplicate_counts has at least one element
                // at that point
                let count = duplicate_counts.last_mut().unwrap();
                *count += 1;
            } else {
                duplicate_counts.push(1);
            }
        } else {
            duplicate_counts.push(1);
        }

        last_item = Some(item);
    }

    vec.dedup();

    duplicate_counts
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_dedup_with_count() {
        let mut vec = vec![1, 2, 2, 3, 3, 3, 4, 4, 4, 4];

        let dup_counts = dedup_with_count(&mut vec);

        assert_eq!(dup_counts, [1, 2, 3, 4]);
        assert_eq!(vec, [1, 2, 3, 4]);
    }
}
