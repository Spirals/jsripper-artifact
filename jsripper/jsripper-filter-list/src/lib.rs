#![forbid(unsafe_code)]
// #![deny(clippy::pedantic)]

use std::{
    fs::File,
    io,
    ops::Range,
    path::{Path, PathBuf},
    time::SystemTime,
};

use serde::{Deserialize, Serialize};
use url::Url;

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct FilterList {
    pub name: String,
    pub description: String,
    pub format_version: String,
    pub modified: SystemTime,
    pub expiration_date: SystemTime,
    pub rules: Vec<FilterRule>,
}

impl FilterList {
    #[must_use]
    pub fn new(
        name: &str,
        description: &str,
        modified: &SystemTime,
        expiration_date: &SystemTime,
    ) -> Self {
        Self {
            name: String::from(name),
            format_version: String::from("0.1.0"),
            modified: *modified,
            expiration_date: *expiration_date,
            description: String::from(description),
            rules: Vec::new(),
        }
    }
}

impl FilterList {
    pub fn from_json(json: &str) -> serde_json::Result<Self> {
        serde_json::from_str(json)
    }

    #[must_use]
    pub fn matches_signature(&self, function_signature: &str) -> bool {
        self.rules.iter().any(|r| {
            if let FilterRule::FunctionSignature(SignatureRule { signature, .. }) = r {
                signature == function_signature
            } else {
                false
            }
        })
    }
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub enum FilterRule {
    FunctionSignature(SignatureRule),
    ByteRange(ByteRangeRule),
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct SignatureRule {
    pub signature: String,
    pub allow: Vec<RuleGuard>,
    pub block: Vec<RuleGuard>,
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct ByteRangeRule {
    pub url: Url,
    pub sha256: String,
    pub byte_ranges: Vec<Range<u32>>,
}

#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub struct RuleGuard {
    url: Option<Url>,
    sha256: Option<String>,
}

#[derive(Debug, Clone)]
pub struct FilterListFile {
    filter_list: FilterList,
    path: PathBuf,
}

impl FilterListFile {
    #[must_use]
    pub fn new(filter_list: FilterList, path: &Path) -> Self {
        Self {
            filter_list,
            path: path.to_path_buf(),
        }
    }

    pub fn from_file(path: &Path) -> Result<Self, io::Error> {
        let file = File::open(path)?;
        let filter_list = serde_json::from_reader(file).unwrap();

        Ok(Self {
            filter_list,
            path: path.to_path_buf(),
        })
    }

    pub fn write(&self) -> serde_json::Result<()> {
        let file = File::create(&self.path).unwrap();
        serde_json::to_writer(file, &self.filter_list)
    }

    pub fn append_rule(&mut self, rule: FilterRule) {
        self.filter_list.rules.push(rule);
    }

    #[must_use]
    pub fn filter_list(&self) -> &FilterList {
        &self.filter_list
    }
}

#[non_exhaustive]
#[derive(Debug, Clone, PartialEq, Eq, Serialize, Deserialize)]
pub enum FunctionBodyShim {
    EmptyArray,
    EmptyObject,
    EmptyString,
    True,
    False,
    Null,
}

impl FunctionBodyShim {
    #[must_use]
    pub fn to_javascript(&self) -> &str {
        match *self {
            Self::EmptyArray => "[]",
            Self::EmptyObject => "{}",
            Self::EmptyString => "''",
            Self::True => "true",
            Self::False => "false",
            Self::Null => "null",
        }
    }
}
