# ---
# jupyter:
#   jupytext:
#     formats: ipynb,py:percent
#     text_representation:
#       extension: .py
#       format_name: percent
#       format_version: '1.3'
#       jupytext_version: 1.14.4
#   kernelspec:
#     display_name: Python 3 (ipykernel)
#     language: python
#     name: python3
# ---

# %% [markdown] tags=[] jp-MarkdownHeadingCollapsed=true
# # General imports

# %% tags=[]
import json
import os
import re

from sqlite3 import connect
import numpy as np
import matplotlib
import matplotlib.pyplot as plt
from matplotlib.ticker import MultipleLocator
import pandas as pd
import seaborn as sns

# %% [markdown] jp-MarkdownHeadingCollapsed=true tags=[] jp-MarkdownHeadingCollapsed=true
# # Matplotlib configuration:

# %% tags=[]
matplotlib.rcParams['pdf.fonttype'] = 42

# %% [markdown] jp-MarkdownHeadingCollapsed=true tags=[] jp-MarkdownHeadingCollapsed=true
# # Pandas configuration:

# %% tags=[]
pd.set_option('display.max_colwidth', None)
pd.set_option('display.max_rows', None)
pd.set_option('styler.latex.hrules', True)
pd.set_option('styler.format.precision', 2)

# %% [markdown] jp-MarkdownHeadingCollapsed=true tags=[] jp-MarkdownHeadingCollapsed=true
# # Seaborn configuration:

# %% tags=[]
custom_params = {'axes.spines.right': False, 'axes.spines.top': False}
sns.set_theme(style='ticks', palette='colorblind', rc=custom_params)

# %% [markdown] jp-MarkdownHeadingCollapsed=true tags=[]
# # Database connection

# %% tags=[]
conn = connect('../crawl_jsripper_data/function_signatures-short.db')

# %% [markdown] tags=[]
# # General statistics

# %% [markdown]
# Number of crawl website domains:

# %% tags=[]
domain_count = pd.read_sql_query(
    '''
    SELECT COUNT(DISTINCT page_domain) FROM data
    ''',
    conn,
    ).iloc[0, 0]
domain_count

# %% [markdown]
# Count of function signatures:

# %%
pd.read_sql_query(
    '''
    SELECT COUNT(function_sig) FROM data
    ''',
    conn,
    ).iloc[0, 0]

# %% [markdown]
# Count of unique function signatures:

# %%
pd.read_sql_query(
    '''
    SELECT COUNT(DISTINCT function_sig) FROM data
    ''',
    conn,
    ).iloc[0, 0]

# %% [markdown] tags=[]
# Count of scripts (including duplicates):

# %% tags=[]
script_count = pd.read_sql_query(
    '''
    SELECT COUNT(DISTINCT resource_url) FROM data
    ''',
    conn,
    ).iloc[0, 0]
script_count

# %% [markdown]
# Count of unique scripts (based on actual, exact content):

# %% tags=[]
unique_script_count = pd.read_sql_query(
    '''
    SELECT COUNT(DISTINCT resource_sha256) FROM data
    ''',
    conn,
    ).iloc[0, 0]
unique_script_count

# %% [markdown]
# Count of unique scripts blocked by filter lists:

# %% tags=[]
unique_tracking_script_count = pd.read_sql_query(
    '''
    SELECT COUNT(DISTINCT resource_sha256) FROM data
    WHERE resource_in_filter_lists = 1
    ''',
    conn,
    ).iloc[0, 0]
unique_tracking_script_count

# %% [markdown]
# Count of unique functions manually marked as tracking:

# %%
unique_tracking_function_count = pd.read_sql_query(
    '''
    SELECT COUNT(DISTINCT function_sig) FROM data
    WHERE origin_script <> '{}'
    ''',
    conn,
    ).iloc[0, 0]
unique_tracking_function_count

# %% [markdown] tags=[]
# Counts of unique scripts per domain:

# %%
script_count_per_domain = pd.read_sql_query(
    '''
    SELECT COUNT(DISTINCT resource_sha256) FROM data
    GROUP BY page_domain
    ''',
    conn,
)

# %%
tracking_script_count_per_domain = pd.read_sql_query(
    '''
    SELECT COUNT(DISTINCT resource_sha256) FROM data
    WHERE resource_in_filter_lists = 1
    GROUP BY page_domain
    ''',
    conn,
    )

# %% tags=[]
script_count_per_domain_both = pd.concat([
    script_count_per_domain.rename(columns={'COUNT(DISTINCT resource_sha256)': 'All scripts'}),
    tracking_script_count_per_domain.rename(columns={'COUNT(DISTINCT resource_sha256)': 'Tracking scripts'}),
], axis=1)

# %%
grid = sns.displot(
    data=script_count_per_domain_both,
    kind='ecdf',
    height=3.8,
    aspect=1.3,
)
grid.set(
    xlabel='Unique script count per domain',
    xscale='log',
    )
grid.ax.yaxis.set_major_locator(MultipleLocator(0.1))
grid.ax.set_xticks(
    [x for x in grid.ax.get_xticks(minor=True) if x >= 1 and x < 10e2],
    minor=True,
    );
sns.move_legend(grid, 'lower right', bbox_to_anchor=(0.7, 0.18))
plt.gcf().savefig('../paper-jsripper/script_count_per_domain.pdf', bbox_inches='tight')

# %%
unique_script_byte_sizes = pd.read_sql_query(
    '''
    SELECT resource_byte_size FROM data
    GROUP BY resource_sha256
    ''',
    conn,
)

# %% tags=[]
unique_tracking_script_byte_sizes = pd.read_sql_query(
    '''
    SELECT resource_byte_size FROM data
    WHERE resource_in_filter_lists = 1
    GROUP BY resource_sha256
    ''',
    conn,
)

# %% tags=[]
unique_script_byte_sizes_both = pd.concat([
    unique_script_byte_sizes.rename(columns={'resource_byte_size': 'All scripts'}),
    unique_tracking_script_byte_sizes.rename(columns={'resource_byte_size': 'Tracking scripts'}),
], axis=1)

# %%
grid = sns.displot(
    data=unique_script_byte_sizes_both,
    kind='ecdf',
    height=3.8,
    aspect=1.3,
)
grid.set(
    xlabel='Script file size (bytes)',
    xscale='log',
    )
grid.ax.yaxis.set_major_locator(MultipleLocator(0.1))
sns.move_legend(grid, 'lower right', bbox_to_anchor=(0.7, 0.18))
plt.gcf().savefig('../paper-jsripper/unique_script_byte_sizes.pdf', bbox_inches='tight')

# %%
function_count_per_script = pd.read_sql_query(
    '''
    SELECT COUNT(function_sig) FROM data
    GROUP BY resource_sha256
    ''',
    conn,
)

# %%
function_count_per_tracking_script = pd.read_sql_query(
    '''
    SELECT COUNT(function_sig) FROM data
    WHERE resource_in_filter_lists = 1
    GROUP BY resource_sha256
    ''',
    conn,
)

# %%
function_count_per_scripts = pd.concat([
    function_count_per_script.rename(columns={'COUNT(function_sig)': 'All scripts'}),
    function_count_per_tracking_script.rename(columns={'COUNT(function_sig)': 'Tracking scripts'}),
], axis=1)

# %%
grid = sns.displot(
    data=function_count_per_scripts,
    kind='ecdf',
    height=3.8,
    aspect=1.3,
)
grid.set(
    xlabel='Function count per script',
    xscale='log',
    )
grid.ax.yaxis.set_major_locator(MultipleLocator(0.1))
grid.ax.set_xticks(
    [x for x in grid.ax.get_xticks(minor=True) if x >= 1 and x < 10e5],
    minor=True,
    );
sns.move_legend(grid, 'lower right', bbox_to_anchor=(0.7, 0.15))
plt.gcf().savefig('../paper-jsripper/function_count_per_scripts.pdf', bbox_inches='tight')

# %%
unique_function_byte_size = pd.read_sql_query(
    '''
    SELECT function_byte_end - function_byte_start FROM data
    GROUP BY function_sig
    ''',
    conn,
)

# %%
grid = sns.displot(
    data=unique_function_byte_size,
    kind='ecdf',
    legend=False,
    height=3.8,
    aspect=1.3,
)
grid.set(
    xlabel='Function code size (bytes)',
    xscale='log',
    )
grid.ax.yaxis.set_major_locator(MultipleLocator(0.1))
plt.gcf().savefig('../paper-jsripper/unique_function_byte_size.pdf', bbox_inches='tight')

# %% [markdown] tags=[]
# # Bundled tracking functions

# %% tags=[]
unblocked_scripts_having_known_tracking_functions = pd.read_sql_query(
    '''
    SELECT * FROM data
    WHERE origin_script <> '{}'
        AND resource_in_filter_lists != 1
    GROUP BY resource_sha256;
    ''',
    conn,
)
unblocked_scripts_having_known_tracking_functions

# %% [markdown]
# Scripts (likely bundles) having at least a function manually marked as tracking and coming from a tracking library:

# %% tags=[]
sample_bundles = unknown_scripts_having_known_tracking_functions[['resource_url', 'origin_script']] \
    .sample(frac=1, random_state=42) \
    .reset_index(drop=True) \
    .truncate(after=10)
sample_bundles['resource_url'] = sample_bundles['resource_url'] \
    .transform(lambda s: s.replace('_', '\\_'))
sample_bundles['origin_script'] = sample_bundles['origin_script'] \
    .transform(lambda s: json.loads(s))
sample_bundles = sample_bundles \
    .rename(columns={'resource_url': 'Scripts containing tracking functions', 'origin_script': 'Known tracking scripts'})
sample_bundles.style.hide(axis='index').to_latex(
    '../paper-jsripper/sample_bundles.tex',
    position='t',
    environment='sidewaystable',
    column_format='ll',
    label='tab:sample_bundles',
    caption='Examples of scripts bundling tracking functions, these scripts are not part of filter lists',
)
sample_bundles[['Scripts containing tracking functions']] \
    .transform(lambda s: s.replace('_', '\_')).style.hide(axis='index').to_latex(
    '../paper-jsripper/sample_bundles_bundle_only.tex',
    position='t',
    environment='table*',
    column_format='ll',
    label='tab:sample_bundles_bundle_only',
    caption='Examples of scripts containing tracking functions, these scripts are not part of filter lists',
)
sample_bundles

# %% [markdown]
# Tracking scripts most often bundled:

# %%
tracking_scripts_most_often_bundled = unblocked_scripts_having_known_tracking_functions['origin_script'] \
    .apply(lambda s: json.loads(s)) \
    .value_counts() \
    .to_frame() \
    .reset_index() \
    .rename(columns={'index': 'Tracking script', 'origin_script': 'Bundle count'})
tracking_scripts_most_often_bundled.style.hide(axis='index').to_latex(
    '../paper-jsripper/tracking_scripts_most_often_bundled.tex',
    position='t',
    environment='table*',
    column_format='lr',
    label='tab:tracking_scripts_most_often_bundled',
    caption='Tracking scripts whose tracking functions are most often bundled',
    )
tracking_scripts_most_often_bundled.truncate(after=5).style.hide(axis='index').to_latex(
    '../paper-jsripper/tracking_scripts_most_often_bundled_top_5.tex',
    position='t',
    environment='table*',
    column_format='lr',
    label='tab:tracking_scripts_most_often_bundled_top_5',
    caption='Tracking scripts whose tracking functions are most often bundled (truncated to top~5)',
)
tracking_scripts_most_often_bundled

# %% [markdown]
# Percentage of unique scripts having bundled tracking script out of unique scripts unknown to filter lists:

# %% tags=[]
100 * unblocked_scripts_having_known_tracking_functions.shape[0] / (unique_script_count-unique_tracking_script_count)

# %% tags=[]
domain_having_tracking_bundle_count = pd.read_sql_query(
    '''
    SELECT COUNT(DISTINCT page_domain) FROM data
    WHERE origin_script <> '{}'
        AND resource_in_filter_lists != 1
    ''',
    conn,
    ).iloc[0, 0]
domain_having_tracking_bundle_count

# %% [markdown]
# Percentage of domains having at least one script (unknown to filter list) having a tracking function:

# %% tags=[]
100 * domain_having_tracking_bundle_count / domain_count

# %% [markdown]
# # Case studies

# %% tags=[]
marked_unblocked_scripts = pd.read_sql_query(
    '''
    SELECT page_domain, resource_url, origin_script FROM data
    WHERE origin_script <> '{}'
        AND resource_in_filter_lists != 1
    GROUP BY resource_url;
    ''',
    conn,
)

# %% tags=[]
marked_unblocked_scripts.to_csv('marked_unblocked_scripts.tsv', sep='\t')

# %% tags=[]
marked_unblocked_scripts

# %%
