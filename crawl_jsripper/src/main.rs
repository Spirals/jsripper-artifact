use std::env;
use std::path::Path;
use std::time::Duration;

use log::{debug, error, info};
use simplelog::{
    ColorChoice, CombinedLogger, ConfigBuilder, LevelFilter, TermLogger, TerminalMode, WriteLogger,
};
use thirtyfour::{
    common::capabilities::desiredcapabilities::PageLoadStrategy, error::WebDriverResult,
    extensions::addons::firefox::FirefoxTools, session::handle::SessionHandle, DesiredCapabilities,
    WebDriver,
};
use tokio::fs;
use url::Url;

const DATA_DIR: &str = "./data";

const INTERACTIVE_TIMEOUT: Duration = Duration::from_secs(15);
const LOAD_TIMEOUT: Duration = Duration::from_secs(45);
const POST_LOAD_TIMEOUT: Duration = Duration::from_secs(30);

const WINDOW_SIZE: (u32, u32) = (1280, 800);

const FILE_PATH_TRUNCATE_LENGTH: usize = 200;

#[tokio::main]
async fn main() {
    const HEADLESS: bool = true;

    CombinedLogger::init(vec![
        TermLogger::new(
            LevelFilter::Debug,
            ConfigBuilder::new()
                .set_time_format_rfc3339()
                .add_filter_allow_str("crawl_jsripper")
                .build(),
            TerminalMode::Mixed,
            ColorChoice::Auto,
        ),
        WriteLogger::new(
            LevelFilter::Debug,
            ConfigBuilder::new()
                .set_time_format_rfc3339()
                .add_filter_allow_str("crawl_jsripper")
                .build(),
            std::fs::File::create("crawl.log").unwrap(),
        ),
    ])
    .unwrap();

    let domain_list_path = fs::canonicalize(
        env::args()
            .nth(1)
            .expect("Please pass the URL list as the first argument"),
    )
    .await
    .unwrap();
    let list = fs::read_to_string(domain_list_path)
        .await
        .expect("Could not read the list file");
    let page_count = list.lines().count();
    info!("{page_count}\u{a0}pages to visit");

    let driver_tools = setup_firefox_driver(4444, &PageLoadStrategy::Eager, HEADLESS)
        .await
        .unwrap();

    for (i, line) in list.lines().enumerate() {
        let page_url = if let Some(arg) = env::args().nth(2) {
            if arg == "--urls" {
                Url::parse(line).unwrap()
            } else {
                unimplemented!();
            }
        } else {
            Url::parse(&format!("http://{line}/")).unwrap()
        };

        info!("Visiting page {page_url}");
        if let Err(err) = download_page_resources(&driver_tools.driver.handle, &page_url).await {
            error!("Error while visiting page {page_url}: {err}");
        } else {
            info!("Completed page {}/{page_count}", i + 1);
        }
    }

    driver_tools.driver.quit().await.unwrap();
}

struct DriverTools {
    driver: WebDriver,
    tools: FirefoxTools,
}

async fn setup_firefox_driver(
    port: u16,
    page_load_strategy: &PageLoadStrategy,
    headless: bool,
) -> WebDriverResult<DriverTools> {
    let mut caps = DesiredCapabilities::firefox();
    caps.set_page_load_strategy(page_load_strategy.clone())?;

    if headless {
        caps.set_headless()?;
    }

    let driver = WebDriver::new(&format!("http://localhost:{port}"), caps).await?;

    let load_timeout = match *page_load_strategy {
        PageLoadStrategy::Eager => INTERACTIVE_TIMEOUT,
        PageLoadStrategy::Normal => LOAD_TIMEOUT,
        #[allow(clippy::unimplemented)]
        PageLoadStrategy::None => unimplemented!(),
    };
    driver.set_page_load_timeout(load_timeout).await?;

    driver
        .set_window_rect(0, 0, WINDOW_SIZE.0, WINDOW_SIZE.1)
        .await?;
    info!("Set the window size to {}x{}", WINDOW_SIZE.0, WINDOW_SIZE.1);

    // No mutex is needed around tools as it is only used to take screenshots, and cannot be used
    // to switch the current window/tab
    let tools = FirefoxTools::new(driver.handle.clone());

    Ok(DriverTools { driver, tools })
}

async fn download_page_resources(session: &SessionHandle, url: &Url) -> WebDriverResult<()> {
    session.goto(url).await?;
    info!("Loaded page {url}");

    let external_script_urls = get_page_external_script_urls(session).await?;
    info!(
        "Found {}\u{a0}external scripts on {url}",
        external_script_urls.len(),
    );

    let host = url.host_str().unwrap();
    let site_dir = Path::new(DATA_DIR).join(host);

    fs::create_dir_all(&site_dir).await?;
    info!("Created directory {}", site_dir.display());

    if let Ok(page_html) = get_page_dom(session).await {
        let mut page_html_file_path = escape_ext4_filepath(url.as_str());
        page_html_file_path.truncate(FILE_PATH_TRUNCATE_LENGTH);
        let page_html_dir = site_dir.join("html");
        let page_html_path = page_html_dir.join(page_html_file_path);

        fs::create_dir_all(&page_html_dir).await?;
        fs::write(page_html_path, page_html).await?;
    }

    let agent = ureq::builder()
        .user_agent("Mozilla/5.0 (Windows NT 10.0; rv:105.0) Gecko/20100101 Firefox/105.0")
        .timeout(LOAD_TIMEOUT)
        .build();
    for external_script_url in external_script_urls {
        if let Ok(js_script) = agent.get(&external_script_url).call() {
            let mut js_script_file_path = escape_ext4_filepath(external_script_url.as_str());
            js_script_file_path.truncate(FILE_PATH_TRUNCATE_LENGTH);
            let js_script_dir = site_dir.join("js");
            let js_script_path = js_script_dir.join(js_script_file_path);

            fs::create_dir_all(&js_script_dir).await?;
            fs::write(js_script_path, js_script.into_string()?).await?;
        }
    }

    Ok(())
}

async fn get_page_external_script_urls(session: &SessionHandle) -> WebDriverResult<Vec<String>> {
    session
        .execute(
            r#"
return Array.from(document.scripts).flatMap((script) => script.src ? [script.src] : []);
          "#,
            Vec::new(),
        )
        .await?
        .convert()
}

async fn get_page_dom(session: &SessionHandle) -> WebDriverResult<String> {
    session
        .execute(
            r#"
return document.documentElement.innerHTML;
"#,
            Vec::new(),
        )
        .await?
        .convert()
}

pub fn escape_ext4_filepath(path: &str) -> String {
    path.replace('/', "\x1a")
}
