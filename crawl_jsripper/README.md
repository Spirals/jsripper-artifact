# `crawl_jsripper`

This repository contains the crawler and data analysis notebook.

## License

Licensed under the Apache-2.0 license (see LICENSE).

Copyright (c) 2023 Inria
